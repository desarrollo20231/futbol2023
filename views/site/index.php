<?php

/** @var yii\web\View $this */

$this->title = 'Futbol Campos';
?>



<div class="body-content">
    <div class="card-container">
        <?php
            foreach ($campos as $model) {
                echo $this->render("_campo", [
                    "model" => $model
                ]);
            }
            ?>
    </div>
</div>